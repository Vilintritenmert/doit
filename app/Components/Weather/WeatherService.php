<?php
declare(strict_types=1);

namespace App\Components\Weather;

class WeatherService {
    /**
     * @var string
     */
    private $token;

    /**
     * WhetherService constructor.
     *
     * @param string $token
     */
    public function __construct(
        string $token
    ) {
        $this->token = $token;
    }

    /**
     * @param string $location
     *
     * @return WeatherInfo
     * @throws ProblemWithConnectionWeatherException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWeatherByLocation(string $location) : WeatherInfo
    {
        $client = new \GuzzleHttp\Client([
            'http_errors' => false
        ]);
        $urlEncodedLocation = urlencode($location);
        $apiUrl = "https://api.openweathermap.org/data/2.5/weather?q={$urlEncodedLocation}&APPID={$this->token}";
        $response = $client->request('GET', $apiUrl);

        if ($response->getStatusCode() !== 200) {
            throw new ProblemWithConnectionWeatherException();
        }

        $decodedResponse = json_decode((string)$response->getBody(), true);

        return new WeatherInfo(
            $decodedResponse
        );
    }

}