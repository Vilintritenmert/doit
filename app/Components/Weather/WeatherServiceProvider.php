<?php
declare(strict_types = 1);

namespace App\Components\Weather;

use Illuminate\Support\ServiceProvider;

class WeatherServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind(WeatherService::class, function () {
            $isWeatherTokenExists = (bool)env('WEATHER_API_TOKEN');

            if (!$isWeatherTokenExists) {
                throw new \Exception('Please add WEATHER_API_TOKEN to .env file');
            }

            return new WeatherService(
                env('WEATHER_API_TOKEN')
            );
        });

    }

}