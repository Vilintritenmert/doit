<?php
declare(strict_types=1);

namespace App\Components\Weather;

class ProblemWithConnectionWeatherException extends \Exception
{

    protected $message = 'Problem with connection';

    protected $code = 500;
    
}