<?php
declare(strict_types=1);

namespace App\Components\Weather;

class WeatherInfo
{
    /**
     * @var string
     */
    private $response;

    public function __construct(string $response)
    {
        $this->setResponse($response);
    }

    /**
     * @return string
     */
    public function getResponse(): string
    {
        return $this->response;
    }

    /**
     * @param string $response
     */
    public function setResponse(string $response)
    {
        $this->response = $response;
    }


}