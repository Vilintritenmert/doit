<?php
declare(strict_types=1);

namespace App\Components\Weather;

class AccessDeniedWeatherException extends \Exception
{

    protected $message = 'Access Denied';

    protected $code = 503;
    
}