<?php
declare(strict_types=1);

namespace App\Components\GitHub;

class AccessDeniedException extends \Exception {

    protected $message = 'Access Denied';

    protected $code = 503;
}