<?php
declare(strict_types=1);

namespace App\Components\GitHub;

class ProblemWithConnectionGitHubException extends \Exception {

    protected $message = 'Problem With Connection';

    protected $code = 500;
}