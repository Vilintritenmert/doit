<?php
declare(strict_types = 1);

namespace App\Components\GitHub;

class GitHubService {
    /**
     * @var string
     */
    private $token;

    /**
     * GitHubService constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @param string $name
     *
     * @return GitHubUser
     * @throws NotValidEmailException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws ProblemWithConnectionGitHubException
     */
    public function getUserInfoByName(string $name): GitHubUser
    {
        $client = new \GuzzleHttp\Client([
            'http_errors' => false
        ]);
        $urlEncodedName = urlencode($name);
        $apiUrl = "http://api.github.com/users/{$urlEncodedName}?access_token={$this->token}";
        $response = $client->request('GET', $apiUrl);

        if ($response->getStatusCode() !== 200) {
            throw new ProblemWithConnectionGitHubException();
        }

        $decodedResponse = json_decode((string)$response->getBody(), true);

        return new GitHubUser(
            array_get($decodedResponse, 'email', ''),
            (string)array_get($decodedResponse, 'location', '')
        );
    }

}