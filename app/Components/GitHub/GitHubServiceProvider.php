<?php
declare(strict_types = 1);

namespace App\Components\GitHub;

use Illuminate\Support\ServiceProvider;

class GitHubServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind(GitHubService::class, function () {
            $isGitHubTokenExists = (bool)env('GIT_HUB_API_TOKEN');

            if (!$isGitHubTokenExists) {
                throw new \Exception('Please add GIT_HUB_API_TOKEN to .env file');
            }

            return new GitHubService(
                env('GIT_HUB_API_TOKEN')
            );
        });

    }

}