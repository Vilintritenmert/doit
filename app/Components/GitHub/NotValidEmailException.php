<?php
declare(strict_types = 1);

namespace App\Components\GitHub;

class NotValidEmailException extends \Exception {

    protected $message = 'Not Valid email';

    protected $code = 500;

}