<?php
declare(strict_types=1);

namespace App\Components\GitHub;

class GitHubUser {

    private $email;

    private $location;

    /**
     * GitHubUser constructor.
     *
     * @param string $email
     * @param string $location
     *
     * @throws NotValidEmailException
     */
    public function __construct(string $email, string $location = '')
    {
        $this->setEmail($email);
        $this->setLocation($location);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @throws NotValidEmailException
     */
    public function setEmail(string $email)
    {
        $isEmail = filter_var($email, FILTER_VALIDATE_EMAIL);

        if (!$isEmail) {
            throw new NotValidEmailException();
        }

        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location)
    {
        $this->location = $location;
    }


}