<?php
declare(strict_types=1);

namespace App\Mail;

use App\Components\Weather\WeatherInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendUserNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var WeatherInfo
     */
    private $weatherInfo;

    /**
     * @var string
     */
    private $message;

    /**
     * Create a new message instance.
     *
     * @param WeatherInfo $weatherInfo
     * @param string      $message
     */
    public function __construct(WeatherInfo $weatherInfo, string $message)
    {
        $this->weatherInfo = $weatherInfo;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send-user-notification')
            ->with([
                'weather' => $this->weatherInfo->getResponse(),
                'text' => $this->message
            ]);
    }
}
