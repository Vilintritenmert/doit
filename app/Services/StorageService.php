<?php
declare(strict_types=1);

namespace App\Services;

use Intervention\Image\ImageManagerStatic as Image;


class StorageService
{

    private CONST STORAGE_ORIGINAL = 'avatars/original';

    private CONST STORAGE_THUMBNAIL = 'avatars/thumbnail';

    private function storePathCheck(string $path)
    {
        if (!file_exists($path)) {
            if (!mkdir($path, 0755, true) && !is_dir($path)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
            }
        }
    }

    /**
     * @param $file
     *
     * @param $fileName
     *
     * @return string
     */
    public function storeOriginal($file, $fileName): string
    {
        $extension = $file->getClientOriginalExtension();
        $fileNameWithExtension =  $fileName . '.'. $extension;
        $publicPath = public_path(self::STORAGE_ORIGINAL . '/' );
        $this->storePathCheck($publicPath);
        $fullPath = $publicPath . $fileNameWithExtension;
        $file->store($fullPath);

        $uriToAvatar = self::STORAGE_ORIGINAL . '/'. $fileNameWithExtension;
        return $uriToAvatar;
    }

    /**
     * @param $file
     *
     * @param $fileName
     *
     * @return string
     */
    public function storeThumbnail($file, $fileName): string
    {
        $extension = $file->getClientOriginalExtension();
        $fileNameWithExtension = $fileName . '.' . $extension;
        $publicPath = public_path(self::STORAGE_THUMBNAIL . '/' );
        $this->storePathCheck($publicPath);
        $fullPath = $publicPath . $fileNameWithExtension;

        $image = Image::make($file->getRealPath());
        $resizedImage = $image->resize(
            250
        );
        $resizedImage->save($fullPath);
        $uriToAvatar = self::STORAGE_THUMBNAIL . '/'. $fileNameWithExtension;

        return $uriToAvatar;
    }

}