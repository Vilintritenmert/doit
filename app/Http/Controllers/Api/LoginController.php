<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Login
     *
     * @bodyParam email string required The email of the user.
     * @bodyParam password string required The password of the post.
     *
     *  @response {
     *      "data": 4,
     *        "api_token": "blablabla",
     *         "avatar": "link_to_avatar"
     *  }
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $user = $this->guard()->user();

            $user->generateToken();

            return response()->json([
                'data' => [
                    'api_token' => $user->api_token,
                    'avatar' => $user->avatar,
                ],
            ]);
        }

        return $this->sendFailedLoginResponse($request);
    }

}