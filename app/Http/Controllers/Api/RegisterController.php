<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\StorageService;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    use RegistersUsers;

    /**
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'avatar' => 'required|image'
        ]);
    }


    /**
     * @param $data
     *
     * @return mixed
     */
    protected function create($data)
    {
        $storageService = resolve(StorageService::class);
        $uniqueName = uniqid('image_', false);

        $avatarOriginalPath = $storageService->storeOriginal($data['avatar'], $uniqueName);
        $avatarThumbnailPath = $storageService->storeThumbnail($data['avatar'], $uniqueName);

        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'avatar_original' => $avatarOriginalPath,
            'avatar_thumbnail' => $avatarThumbnailPath,
        ]);
    }

    public function registered(Request $request, $user)
    {
        $user->generateToken();

        return response()->json([
            'data' => [
                'api_token' => $user->api_token,
                'avatar' => $user->avatar,
            ],
        ], 201);
    }

}