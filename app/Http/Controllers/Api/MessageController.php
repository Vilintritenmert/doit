<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Components\GitHub\GitHubService;
use App\Components\GitHub\NotValidEmailException;
use App\Components\Weather\WeatherInfo;
use App\Components\Weather\WeatherService;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendMessagePost;
use App\Mail\SendUserNotification;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller {
    /**
     * @var GitHubService
     */
    private $gitHubService;

    /**
     * @var WeatherService
     */
    private $weatherService;

    /**
     * MessageController constructor.
     *
     * @param GitHubService  $gitHubService
     * @param WeatherService $weatherService
     */
    public function __construct(
        GitHubService $gitHubService,
        WeatherService $weatherService
    ) {
        $this->gitHubService = $gitHubService;
        $this->weatherService = $weatherService;
    }

    /**
     * TODO: Add queues
     *
     * @param SendMessagePost $messagePost
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Components\GitHub\ProblemWithConnectionGitHubException
     * @throws \App\Components\Weather\ProblemWithConnectionWeatherException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(SendMessagePost $messagePost): \Illuminate\Http\JsonResponse
    {
        $names = array_merge([], (array)$messagePost->input('name'));
        $response = [];

        foreach($names as $name) {
            try {
                $gitHubUserInfo = $this->gitHubService->getUserInfoByName($name);

                $weatherInfo = new WeatherInfo('User Didn`t have location info');
                if ($gitHubUserInfo->getLocation()) {
                    $weatherInfo = $this->weatherService->getWeatherByLocation($gitHubUserInfo->getLocation());
                }

                $message = htmlspecialchars($messagePost->input('message'));

                Mail::to($gitHubUserInfo->getEmail())->send(new SendUserNotification($weatherInfo, $message));

                $response[] = [
                    'data' => [
                        'name' => $name,
                        'email' => $gitHubUserInfo->getEmail()
                    ]
                ];

            } catch(NotValidEmailException $exception) {
                $response[] = [
                    'data' => [
                        'name' => $name,
                        'error' => $exception->getMessage()
                    ]
                ];
            }
        }

        return response()->json(
            $response
        );
    }
}

