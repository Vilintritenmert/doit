#Installation 

1. copy `.env.example` to `.env`
2. `/etc/hosts` >> `127.0.0.1 doit.loc` 
3. edit `.env`
    1. `.env` add GIT_HUB_API_TOKEN 
    2. `.env` add WEATHER_API_TOKEN
4. `docker-compose build` 
5. `docker-compose run --rm --no-deps doit_app php artisan key:generate`
6. `docker-compose run --rm --no-deps doit_app php artisan migrate`
7. `docker-compose up` 
8. enjoy => http://doit.loc


## Documentation

### SignUp 

Url: `/api/register`

```
method POST

@param $email User Email 
@param $password User Password
@param $avatar Image

@response 
{
    'data':{
        'avatar':'/pathToAvatar...',
        'api_token':'blablablabla'    
    } 
}

```

### SignIn 

Url: `/api/login`

```
method POST

@param $email User Email 
@param $password User Password

@response 
{
    'data':{
        'avatar':'/pathToAvatar...',
        'api_token':'blablablabla'    
    } 
}

```

### Send Message

Url: `/api/send-message`

```
method POST

@header 'Authorization':'Bearer $token'

@param $name Name of User GitHub String or Array of string  
@param $message Message for email 

@response 
{
    'data':[{
        'name':'nameOfUserOnGitHub',
        'email':'emailFromGitHub@bla.com'
    }]
}

```
