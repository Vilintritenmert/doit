#!/bin/bash

envsubst "`for v in $(compgen -v); do printf '${%s} ' $v; done`'" \
    < /etc/nginx/conf.d/website.template \
    > /etc/nginx/conf.d/default.conf

nginx -g 'daemon off;'
