#!/bin/bash

find /app/ -type f -exec chmod 644 {} \;
find /app/ -type d -exec chmod 755 {} \;
chgrp -R www-data storage bootstrap/cache
chmod -R ug+rwx storage bootstrap/cache
chown -R www-data:www-data /app/public/uploads

php-fpm